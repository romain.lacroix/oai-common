# Sharing code between Network Functions

One way to do it with our development tools is git submodules. The main benefit from this approach is that the synchronisation of versions / branches / tags is handled by the well-supported versioning tool, we can then use it instead of syncComponents.sh.

## Executive summary

#### Update an NF to the latest version of the common submodule: we just change the git head in the directory of the submodule, then commit it.
```
~/OAI/oai-cn5g-amf/component/common $ git checkout origin/develop
~/OAI/oai-cn5g-amf/component/common $ cd ../..
~/OAI/oai-cn5g-amf $ git commit component/common -m "update sha1"
```

#### Get the request version of the submodule (it may have advanced with a git merge or rebase).
```
~/OAI/oai-cn5g-amf $ git submodule update --init
```

## The concept: sharing source code rather than compiled libraries

The shared component is available in source-code form, so that we can then `#include` it. We use CMake directives to see the include paths, and we include shared Bash scripts with the `source` directive.

These files are very similar and would be rationalized:
- `*/src/common`
- `*/build/scripts/build_helper*`
- `*/docker/Dockerfile*`

This solution is better than sharing a compiled library for compatibility: it does not need to match the ABI.

As for "should we rebuild all NFs when the shared module advances?", I think it is not needed with source-code sharing, because the NFs can live with different versions of the module (as is already the case with manually copied files).

## Example

The file `uint_generator.hpp` is similar in the AMF and SMF repositories. It has been moved to an `oai-common` repository, which is a submodule of the two NF repositories.

See the differences in [AMF](https://gitlab.eurecom.fr/oai/cn5g/oai-cn5g-amf/-/compare/develop...poc_common_submodule?from_project_id=2363&straight=false&w=1)

See the differences in [SMF](https://gitlab.eurecom.fr/oai/cn5g/oai-cn5g-smf/-/compare/develop...poc_common_submodule?from_project_id=2364&straight=false&w=1)

## Open questions

- Where should we put the shared module? `/component` ?
- Is the `--recursive` option needed for `git submodule update`? Probably yes for the `-fed` repo which will have multiple floors of submodules.
- Can we remove the `syncComponents.sh` scripts and rely on git submodules / sha1 for synchronising?
- Do we decide to advance all NFs when the shared module advances?
- On repository setup, should we add the submodule with the SSH or with the HTTPS url? Maybe SSH for developers and HTTPS for users.

## Useful git options

When using git submodules, these options make `git status` and `git diff` give more useful output when a submodule has changed from the head.

In `~/.gitconfig`
```
[diff]
    submodule = log
[status]
    submoduleSummary = true
```

Output of these git commands with the submodule options :

```
$ git status
On branch poc_common_submodule
Your branch is up to date with 'origin/poc_common_submodule'.

Changes not staged for commit:
	modified:   component/oai-common (new commits, modified content)

Submodules changed but not updated:

* component/oai-common 6651663...968cb49 (1):
  > blurb explaining the motivation behind git submodules

$ git diff
Submodule component/oai-common contains modified content
Submodule component/oai-common 66516631..968cb49f:
  > blurb explaining the motivation behind git submodules
```
